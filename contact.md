---
pagetitle: Laurent Duchemin's Webpage
navbar_contact: true
---

<!----------------->

| **Laurent Duchemin**
|     [PMMH](https://www.pmmh.espci.fr/) – UMR 7636
|     Sorbonne Université
|     Barre Cassan bat A, 1er étage Case 18
|     7 Quai Saint Bernard
|     75005 Paris, France

**E-mail** : laurent.duchemin at espci.fr

<!--| Laurent Duchemin-->
<!--| [PMMH](https://www.pmmh.espci.fr/)-->
<!--| E-mail: laurent dot duchemin at espci dot fr-->
<!--| Office: building, room-->
<!--| Phone: number-->
