---
pagetitle: Laurent Duchemin's Webpage
navbar_divers: true
---

<div class="navbar">
[Divers](divers.html) / Section 3
<!--[Home](index.html) / Section 3-->
</div>

3rd section
==========


There can be sub-section pages as well. [This one](section3_sub1.html) is an example. 

Comprehensive guide to Pandoc's markdown is
[here](http://pandoc.org/README.html#pandocs-markdown).
