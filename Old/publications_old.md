---
pagetitle: Laurent Duchemin's Webpage
navbar_pubs: true
---

-------------

# Preprints coming soon

# Preprints

- **MARS : A Method for the Adaptive Removal of Stiffness in PDEs** ([PDF](../Papers/Stiff.pdf))\
L. Duchemin & J. Eggers

# Reprints

- Rayleigh-Bénard convection with a melting boundary
B. Favier, J. Purseed & L. Duchemin [PDF](../Papers/fpd.pdf)
- Tree crowns grow into self-similar shapes controlled by gravity and light sensing</h7>
L. Duchemin, C. Eloy, E. Badel & B. Moulia,
- The diffusive sheet method for scalar mixing, D. Martinez-Ruiz, P. Meunier, B. Favier, L. Duchemin & E. Villermaux, 2018,
- Impact on floating membranes</h7>
	(<a href="PDF/vd.pdf">PDF</a>)<br>
	N. Vandenberghe & L. Duchemin, 2016,
	<EM><a href="http://journals.aps.org/pre/abstract/10.1103/PhysRevE.93.052801"> Physical Review E.</a></EM>,
	vol. 93, 052801.
	</li>

	<li>
	<h7>Self-similar impulsive capillary waves on a ligament</h7>
	(<a href="PDF/dlvv.pdf">PDF</a>)<br>
	L. Duchemin, S. Le Diz&egrave;s, L. Vincent & E. Villermaux, 2015,
	<EM><a href="http://scitation.aip.org/phf/">Phys. Fluids</a></EM>, 
	vol. 27, 051704.
	</li>

	<li>
	<h7>Forced dynamics of a short viscous liquid bridge</h7>
	(<a href="PDF/vdl.pdf">PDF</a>)<br>
	L. Vincent, L. Duchemin & S. Le Diz&egrave;s, 2014, 
	<EM><a href="http://128.232.233.5/action/displayJournal?jid=FLM">J. Fluid  Mech.</a></EM>, 
	vol. 761, 220--240.
	</li>

	<li>
	<h7>Impact dynamics for a floating elastic membrane</h7>
	(<a href="PDF/dv.pdf">PDF</a>)<br>
	L. Duchemin & N. Vandenberghe, 2014,
	<EM><a href="http://128.232.233.5/action/displayJournal?jid=FLM">J. Fluid  Mech.</a></EM>, 
	vol. 756, 544--554.
	</li>

	<li>
	<h7>Two dimensional Leidenfrost Droplets in a Hele Shaw Cell</h7>
	(<a href="PDF/cfcrdp.pdf">PDF</a>)<br>
	F. Celestini, T. Frisch, A. Cohen, C. Raufaste, L. Duchemin & Y. Pomeau, 2014, <br> 
  	<EM><a href="http://scitation.aip.org/phf/">Phys. Fluids</a></EM>,
	vol. 26, 032103.
	</li>

	<li>
	<h7>Remnants from fast liquid withdrawal</h7>
	(<a href="PDF/vdv.pdf">PDF</a>)<br>
	L. Vincent, L. Duchemin & E. Villermaux, 2014,
  	<EM><a href="http://scitation.aip.org/phf/">Phys. Fluids</a></EM>,
	vol. 26, 031701.
	</li>

	<li>
	<h7>The Explicit-Implicit-Null method: removing the numerical instability of PDEs</h7>
	(<a href="PDF/de.pdf">PDF</a>)<br>
	L. Duchemin & J. Eggers, 2014,  
  	<EM><a href="http://http://www.sciencedirect.com/science/journal/00219991">J. Comput. Phys.</a></EM>,
	vol. 263, 37--52.
	</li>

	<li>
	<h7>Rarefied gas correction for the bubble entrapment singularity in drop impacts</h7>
	(<a href="PDF/dj2.pdf">PDF</a>)<br>
	L. Duchemin & C. Josserand, 2012, 
  	<EM><a href="http://www.sciencedirect.com/science/journal/16310721">C. R. Mecanique</a></EM>,
	vol. 340, 797--803.
	</li>

	<li>
	<h7>Asymptotic behavior of a retracting two-dimensional fluid sheet</h7>
	(<a href="PDF/gadj.pdf">PDF</a>)<br>
	L. Gordillo, G. Agbaglah, L. Duchemin, & C Josserand, 2011,
  	<EM><a href="http://scitation.aip.org/phf/">Phys. Fluids</a></EM>,
	vol. 23, 122101.
	</li>

	<li>
	<h7>Curvature singularity and film-skating during drop impact</h7>
	(<a href="PDF/dj1.pdf">PDF</a>)<br>
	L. Duchemin & C. Josserand, 2011, 
  	<EM><a href="http://scitation.aip.org/phf/">Phys. Fluids</a></EM>,
	vol. 23, 091701 (2011).
	</li>

	<li>
	<h7>Shape and stability of axisymmetric levitated viscous drops</h7>
	(<a href="PDF/ltpd.pdf">PDF</a>)<br>
	J.R. Lister, A. B. Thompson, A. Perriot & L. Duchemin, 2008, 
	<EM><a href="http://128.232.233.5/action/displayJournal?jid=FLM">J. Fluid  Mech.</a></EM>, 
	vol. 617, 167--185.
	</li>

	<li>
	<h7>Long-time evolution of inviscid thin jets</h7>
	(<a href="PDF/d.pdf">PDF</a>)<br>
	L. Duchemin, 2008, 
	<EM><a href="http://rspa.royalsocietypublishing.org/">Proc. R. Soc. A</a></EM>, 
	vol. 464, no 2089, 197--206.
	</li>

	<li>
	<h7>Ablative Rayleigh-Taylor instability for strong temperature dependence of thermal conductivity</h7>
	(<a href="PDF/acds.pdf">PDF</a>)<br>
	C. Almarcha, P. Clavin, L. Duchemin & J. Sanz, 2007, 
	<EM><a href="http://128.232.233.5/action/displayJournal?jid=FLM">J. Fluid  Mech.</a></EM>, 
	vol. 579, 481--492.
	</li>

	<li>
	<h7>Asymptotic behaviour of the Rayleigh-Taylor instability</h7>
	(<a href="PDF/djc.pdf">PDF</a>)<br>
	L. Duchemin, C. Josserand & P. Clavin, 2005, 
	<EM><a href="http://prl.aps.org/">Phys. Rev. Lett.</a></EM>, 
	94, 224501.
	</li>

	<li>
	<h7>Static shapes of a levitated viscous drop</h7>
	(<a href="PDF/dll.pdf">PDF</a>)<br>
	L. Duchemin, J. Lister & U. Lange, 2005, 
	<EM><a href="http://128.232.233.5/action/displayJournal?jid=FLM">J. Fluid  Mech.</a></EM>, 
	vol. 533, 161--170.
	</li>

	<li>
	<h7>The effect of solid boundaries on pore shrinkage in Stokes flow</h7>
	(<a href="PDF/cd.pdf">PDF</a>)<br>
	D. Crowdy & L. Duchemin, 2005, 
	<EM><a href="http://128.232.233.5/action/displayJournal?jid=FLM">J. Fluid  Mech.</a></EM>, 
	vol. 531, 359--379.
	</li>

	<li>
	<h7>Inviscid coalescence of drops</h7>
	(<a href="PDF/dej.pdf">PDF</a>)<br>
	<!-- <b><a href="PDF/dej.pdf">Inviscid coalescence of drops</a></b><br> -->
	L. Duchemin, J. Eggers & C. Josserand, 2003, 
	<EM><a href="http://128.232.233.5/action/displayJournal?jid=FLM">J. Fluid  Mech.</a></EM>, 
	vol. 487, 167--178.
	</li>

	<li>
	<h7>Pyramidal and toroidal water drops after impact on a solid surface</h7>
	(<a href="PDF/Pyramids.pdf">PDF</a>)<br>
	Renardy, Y., Popinet, S., Duchemin, L., Renardy, M., Zaleski, S., Josserand, C., Drumright-Clarke, M.A., Richard, D., Clanet, C., Qu&eacute;r&eacute;, D.
	<EM><a href="http://128.232.233.5/action/displayJournal?jid=FLM">J. Fluid  Mech.</a></EM>, 
  	<B>484</B>, 69--83 (2003).
	</li>

	<li>
	<h7>Jet Formation in gas bubbles bursting at a free surface</h7>
	(<a href="PDF/dpjz.pdf">PDF</a>)<br>
	Duchemin, L., Popinet, S., Josserand, C., Zaleski, S.
  	<EM><a href="http://scitation.aip.org/phf/">Phys. Fluids</a></EM>,
  	<B>14</B>, 3000 (2002).
	</li>
        </ul>
