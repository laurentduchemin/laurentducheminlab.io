---
pagetitle: Laurent Duchemin's Webpage
navbar_resume: true
---

# Career

-   Since 2019 : Professor, [ESPCI](https://www.espci.psl.eu/en/), Paris, France.
-   2005-2019 : Assistant Professor, [Aix-Marseille Université](http://www.univ-amu.fr/), Marseille, France.
-   2003-2005 : Post-doctoral researcher, [DAMTP](http://www.damtp.cam.ac.uk/), Cambridge, UK.
-   2002-2003 : Post-doctoral researcher, [Imperial College](http://www3.imperial.ac.uk/), London, UK.
-   2001-2002 : Post-doctoral researcher, IFP, Rueil-Malmaison, France.
-   December 2001 : PhD, Université Aix-Marseille II.

# PhD students

-   Aliénor Rivière : Since September 2021.
-   Keshav PURSEED : PhD defended in December 2020.
-   Lionel VINCENT : PhD defended in December 2013.

# Collaborators

- [Eric Badel](https://www6.clermont.inrae.fr/piaf_eng/About/Staff/MECA-staff)
- [José Bico](https://blog.espci.fr/jbico/)
- [Jens Eggers](https://people.maths.bris.ac.uk/~majge/)
- [Christophe Eloy](https://www.irphe.fr/~eloy/)
- [Benjamin Favier](https://sites.google.com/site/bfavierhome/)
- [Christophe Josserand](http://www.off-ladhyx.polytechnique.fr/people/josserand/)
- [Stéphane Le Dizès](https://www.irphe.fr/~ledizes/)
- [Bruno Moulia](https://www6.clermont.inrae.fr/piaf_eng/About/Staff/MECA-staff)
- [Stéphane Perrard](https://stephaneperrard.wixsite.com/sperrard)
- [Etienne Reyssat](https://www.espci.psl.eu/fr/annuaire?unique_id=AxpcEiEBADo%3D&service=pmmh)
- [Lydie Staron](http://www.lmm.jussieu.fr/~staron/index.html)
- [Nicolas Vandenberghe](https://www.univ-amu.fr/fr/public/bureau-de-linstitut)
- [Emmanuel Villermaux](https://www.iufrance.fr/les-membres-de-liuf/membre/418-emmanuel-villermaux.html)

